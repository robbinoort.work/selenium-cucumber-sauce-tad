@run
Feature: Login to the sauce demo website

  Scenario: Logon with a locked account
    Given I am on the login page of the sauce demo website
    When I login with a locked account
    Then I should see an error message

