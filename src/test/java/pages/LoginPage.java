/*
 * Page objects are deined in separate classes per page or per sub-page.
 * Each page class defines private variables for the screen elements
 * and public methods for the interaction with the screen elements.
 */
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 *
 * @author handringa
 */
public class LoginPage {

    WebDriver driver;

    public LoginPage(WebDriver driver) {
        // The constructor initializes a local driver and variables for the screen elements.
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    //==============================================================================
    // Definition of the screen elements.
    //==============================================================================
    


    //==============================================================================
    // Definition of methods that perform the interaction with the screen elements.
    //==============================================================================
    


}
